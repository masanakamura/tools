#!/bin/bash
#
# create a ssh config file by reading a hostname  ipaddress list
#
input="lab_list.txt"
output="config.output"
while IFS=$'\t' read -r host ip_address 
do
   echo "
#
Host $host
Hostname $ip_address
User ansible
IdentityFile ~/.ssh/ansible.id_rsa.lab
ServerAliveInterval 240" >> $output
done < $input
exit

